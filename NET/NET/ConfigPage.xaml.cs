﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace NET
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class ConfigPage : Page
    {
        public ConfigPage()
        {
            this.InitializeComponent();
        }

        public bool? IsSelected { get; private set; }
        //public System.Windows VerticalAlignment { get; set; }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var computerConfig = new[]
            {
                ////Professionel
                new[] {
                    new[] { "Processor", "Core i5 7640X", "Core i7 7740X", "Core i7 7800X" },
                    new[] { "Motherboard", "X299 RAIDER", "X299 SLI PLUS", "PRIME X299-A" },
                    new[] { "SSD", "A400 - 120GB", "SSD UV400 - 240 Go", "MX300 - 275 Go" },
                    new[] { "GraphicalCard", "GT 710 LP - 1 Go", "GeForce GTX 1050 Ti AERO ITX OC V1 - 4 Go", "Quadro P600 - 2 Go" },
                    new[] { "Memory", "4 Go", "6 Go" },
                    new[] { "HardDisk", "1 To - SkyHawk", "2 To - FireCuda" },
                    new[] { "Core", "4 Core", "6 Core" }
                },

                ////Bureatique
                new[] {
                    new[] { "Processor", "Intel Celeron G3930", "Processeur Intel Pentium/Core i3", "Celeron G4900" },
                    new[] { "Motherboard", "ASRock H110M-DGS R3", "H310M PRO-M2" },
                    new[] { "SSD", "Kingston UV400 240 Go", "Kingston 128 Go" },
                    new[] { "GraphicalCard", "NVIDIA GT/GTX", "AMD Radeon R2", "GeForce GT 1030 AERO ITX OC" },
                    new[] { "Memory", "4 Go", "6 Go" },
                    new[] { "HardDisk", "1 To - SkyHawk","500 Go - FireCuda" },
                    new[] { "Core", "4 Core", "6 Core" }
                },

                ////Gamer
                new[] {
                    new[] { "Processor", "Intel Core i7 8700K", "Ryzen 5 2600X", "Ryzen 7 2700X" },
                    new[] { "Motherboard", "X370 GAMING PRO CARBON", "X370 KRAIT GAMING", "X470 Gaming PLUS" },
                    new[] { "SSD", "SSD UV400 - 240 Go", "MX300 - 275 Go", "1 To - MX300 M.2 Type 2280" },
                    new[] { "GraphicalCard", "GeForce GTX 1060 Gaming X - 6 Go", "GeForce GTX 1070 STRIX GAMING - 8 Go", "GeForce GTX 1080 Ti Gaming X - 11 Go" },
                    new[] { "Memory", "Fury DDR4 2 x 8 Go 2133 MHz CAS 14", "RipJaws 5 Series Rouge DDR4 2 x 16 Go 3000 MHz CAS 15" },
                    new[] { "HardDisk", "1 To - SkyHawk", "2 To - FireCuda SSHD", "6 To - IronWolf" },
                    new[] { "Core", "6 Core", "8 Core" }
                }
            };

            CheckBox composant;
            //int z = 190;
            switch (e.Parameter as string)
            {
                case "Gamer":
                    for (int y = 0; y < computerConfig[2].Length; y++)
                    {
                        for (int j = 0; j < computerConfig[2][y].Length; j++)
                        {
                            if (j == 0)
                            {
                                composant = new CheckBox
                                {
                                    Content = computerConfig[2][y][j],
                                    IsEnabled = false,
                                };
                            }
                            else
                            {
                                composant = new CheckBox
                                {
                                    Content = computerConfig[2][y][j],
                                    FontSize = 12,
                                    IsChecked = false,
                                    
                                };
                            }
                            this.ListConfig.Items.Add(composant);
                        }
                    }
                    break;
                case "Bureautique":
                    for (int y = 0; y < computerConfig[1].Length; y++)
                    {
                        for (int j = 0; j < computerConfig[1][y].Length; j++)
                        {
                            if (j == 0)
                            {
                                composant = new CheckBox
                                {
                                    Content = computerConfig[1][y][j],
                                    IsEnabled = false
                                };
                            }
                            else
                            {
                                composant = new CheckBox
                                {
                                    Content = computerConfig[1][y][j],
                                    FontSize = 12

                                };
                            }
                            this.ListConfig.Items.Add(composant);
                        }
                    }
                    break;
                case "Professionnel":
                    for (int y = 0; y < computerConfig[0].Length; y++)
                    {
                        for (int j = 0; j < computerConfig[0][y].Length; j++)
                        {
                            if (j == 0)
                            {
                                composant = new CheckBox
                                {
                                    Content = computerConfig[0][y][j],
                                    IsEnabled = false
                                };
                            }
                            else
                            {
                                composant = new CheckBox
                                {
                                    Content = computerConfig[0][y][j],
                                    FontSize = 12
                                };
                            }
                            this.ListConfig.Items.Add(composant);
                        }
                    }
                    break;
            }
        }

        private void PreviousLinkButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (CheckBox item in ListConfig.Items)
            {
                if (item.IsChecked == true)
                {
                    dictionary.Add(i.ToString(), item.Content.ToString());
                    i++;
                }
            }
            this.Frame.Navigate(typeof(ResumPage), dictionary);
        }
    }
}
